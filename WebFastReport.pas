unit WebFastReport;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls,Forms,
  Dialogs, StdCtrls,NativeXml,ReportData, Grids, DBGrids, DB,SysComm,PerlRegEx,
  frxClass, frxDesgn, frxBarcode,frxDBSet,DBClient,ReportDataLoader;
type
  TWebFastReport = class
  private
     Owner:TComponent;

     URL:WideString;
     AppName:WideString;
     ReportName:WideString;

     DataSetName:string;
     ReportData:TReportData;
     ReportDataLoader:TReportDataLoader;
     DataStream:TStream;
     DesignStream:TStream;
     DataSet:RDataSet;
     Variable:RVariable;
     FrxReport:TfrxReport;
     FrxDBDataSet:TFrxDBDataSet;
     MasterData:TFrxMasterData;
     ClientDataSet:TClientDataSet;

     procedure init();
     function OnSaveReport(Report: TfrxReport;SaveAs: Boolean): Boolean;
     procedure OnGetValue(const VarName: String;var Value: Variant);
  public
     constructor create(Aowner:TComponent;AappName:WideString;AreportName:WideString;AUrl:WideString);
     destructor destroy(); override;
     procedure print();
     
  end;
implementation

{ TWebFastReport }

constructor TWebFastReport.create(Aowner:TComponent;AappName:WideString; AreportName: WideString;AUrl: WideString);
begin
  Self.Owner:=Aowner;
  Self.AppName:=AappName;
  Self.ReportName:=AreportName;
  self.URL:=AUrl;
end;

procedure TWebFastReport.print;
begin
  Self.init();
  Self.FrxReport.DesignReport;
end;

destructor TWebFastReport.destroy;
begin
   
end;

procedure TWebFastReport.init;
var
  i,j:Integer;
  flag:Boolean;
begin
  flag:=False;
  Self.ReportData:= TReportData.Create(Self.Owner);
  try
    Self.ReportDataLoader:=TReportDataLoader.create(Self.Owner,Self.AppName,Self.ReportName,Self.URL);
  except
    ShowMessage('000000');
  end;
  Self.DataStream:=Self.ReportDataLoader.GetDataStream;
  Self.DesignStream:=Self.ReportDataLoader.GetDesignStream;
  if Assigned(Self.DataStream) then
  begin
    Self.ReportData.load(Self.DataStream);
  end;
  if Assigned(Self.FrxReport) then
  begin
      FreeAndNil(self.FrxReport);
  end;
  Self.FrxReport:=TfrxReport.Create(self.Owner);
  Self.FrxReport.OnGetValue := Self.OnGetValue;
  Self.FrxReport.LoadFromStream(Self.DesignStream);
  //报表变量加载
  for i := 0 to Self.ReportData.getVariableCount -1  do
  begin
    if i=0 then Self.FrxReport.Variables.Add.Name:= ' def';
    Self.Variable:=Self.ReportData.getRVariable(i);
    Self.FrxReport.Variables.AddVariable('def',Self.Variable.key,Self.Variable.value);
  end;

  //报表数据集加载
  for  i:=0  to Self.ReportData.getDataSetCount-1 do
  begin
    Self.DataSet:=Self.ReportData.getRDataSet(i);
    Self.DataSetName:=Self.DataSet.name;
    Self.ClientDataSet:=Self.DataSet.dataset;

    for j := 0 to self.Owner.ComponentCount-1 do
    begin
       if (Self.Owner.Components[j] is  TFrxDBDataset) and  (TFrxDBDataset(Self.Owner.Components[j]).Name = Self.DataSetName) then
       begin
          flag:=True;
          FrxDBDataSet := TFrxDBDataset(Self.Owner.Components[j]);
       end;
    end;

    if not flag then
    begin
      Self.FrxDBDataSet:=TFrxDBDataset.Create(self.Owner);
      Self.FrxDBDataSet.Name:=Self.DataSetName;
      Self.FrxDBDataSet.UserName:=Self.DataSetName;
    end;
    flag:=False;
    self.FrxDBDataSet.DataSet:=Self.ClientDataSet;
    Self.FrxReport.DataSets.Add(Self.FrxDBDataSet);
    
    Self.MasterData:= TFrxMasterData(Self.FrxReport.FindObject('MasterData_'+Self.DataSetName));
    if Assigned(Self.MasterData) then
    begin
     Self.MasterData.DataSet:=Self.FrxDBDataSet;
    end;
  end;
  Self.DataStream.Free;
  Self.DesignStream.Free;
  self.ReportDataLoader.Free;
  Self.ReportData.destroy;
end;

procedure TWebFastReport.OnGetValue(const VarName: String;  var Value: Variant);
var
  i:Integer;
  vv:RVariable;
begin
  for i := 0 to Self.ReportData.getVariableCount-1  do
  begin
    vv:=Self.ReportData.getRVariable(i);
    if VarName = vv.key then Value:= vv.value;
  end;
end;

function TWebFastReport.OnSaveReport(Report: TfrxReport;SaveAs: Boolean): Boolean;
var
  i:Integer;
  MasterDataName:string;
  Component:TfrxComponent;
  Path:WideString;
begin
  Component:=nil;
  if true then
  begin
     //清除变量
     for i := 0 to Report.Variables.Count-1 do
     begin
        Report.Variables.DeleteVariable(Report.Variables.items[0].Name);    //使用items[0],因为删除时，变量个数在变化
     end;
     //清除数据集
     for  i:=0  to Report.DataSets.Count-1 do
     begin
        MasterDataName:='MasterData_'+Report.DataSets.Items[i].DataSetName;
        Component:=Report.FindObject(masterDataName);
        if Assigned(Component) and (Component is TfrxMasterData) then
        begin
            TfrxMasterData(Component).DataSet:=nil;
        end;
     end;
     Report.DataSets.Clear;
     Path:=GetEnvironmentVariable('USERPROFILE')+'\WebFastReport\'+self.AppName;
     if not DirectoryExists(Path) then
        ForceDirectories(Path);
     Report.SaveToFile(Path+'\'+self.ReportName+'.fr3');
  end;
  result:=true;
end;

end.
