unit ReportDataLoader;

interface
uses
  SysUtils,Dialogs,Classes,ICS_HttpProt,NativeXml,SysComm,PerlRegEx,StrUtils;

type
  TReportDataLoader = class
     private
       Owner:TComponent;
       URL:string;
       ReportName:string;
       AppName:string;
       Params:string;
       OnlyData:Boolean;


       HttpClient:THttpCli;
       HttpSendStream:TStream;
       HttpReceStream:TStream;

       ReponseText:string;
       DesignText:string;
       DataText:string;
       
       procedure OnRequestDone(Sender : TObject;RqType : THttpRequest;ErrCode  : Word);
       procedure Load();
       function  EncodeParams(AParams:string):string;
       function  UrlEncodeParams(AUrl:string):string;

     public           
       constructor create(Aowner:TComponent;AappName:string;AreportName: string;AUrl:string;AParams:string='';IsCreate:Boolean=False);
       destructor destroy(); override;
       function GetDesignStream():TStream;
       function GetDesignText():string;
       function GetDataStream():TStream;
       function GetDataText():string;
       function GetResponseText():string;
       class function GetClientSaveRootDirectory():string;
       class function GetClientSaveDirectory(AappName:WideString):string;
       class function GetClientSavePath(AappName:WideString;AreportName:WideString):string;
      
  end;

implementation

{ TReportDataLoader }

constructor TReportDataLoader.create(Aowner:TComponent;AappName:string;AreportName: string;AUrl:string;AParams:string;IsCreate:Boolean);
var
  postData:string;
begin
  self.Owner:=Aowner;
  Self.AppName:=AappName;
  Self.ReportName:= AreportName;
  Self.URL:=Self.UrlEncodeParams(AUrl);
  Self.Params:= self.EncodeParams(AParams);
  if IsCreate or FileExists(Self.GetClientSavePath(self.AppName,Self.ReportName)) then
  begin
    Self.OnlyData:=True;
  end
  else
  begin
    Self.OnlyData:=False;
  end;
  
  Self.HttpSendStream:= TMemoryStream.Create;

  postData:=self.Params+'appName='+UrlEncode(UTF8Encode(Self.AppName))+'&reportName='+UrlEncode(UTF8Encode(Self.ReportName))+'&onlyData=';
  if self.OnlyData then
  begin
    postData:=postData+'true';
  end
  else
  begin
    postData:=postData+'false';
  end;
  Self.HttpSendStream.Write(postData[1],Length(postData));
  Self.HttpReceStream:= TMemoryStream.Create;
  Self.ReponseText:='100000';
  Self.Load;
end;

function TReportDataLoader.GetDataText: string;
begin
  Result:=self.DataText;
end;

function TReportDataLoader.GetDataStream: TStream;
var
  stream:TStream;
begin
   stream:=TStringStream.Create(Self.DataText);
   Result:=stream;
end;

function TReportDataLoader.GetDesignText: string;
begin
  Result:=self.DesignText;
end;

function TReportDataLoader.GetDesignStream: TStream;
var
  stream:TStream;
begin
  if Self.OnlyData then
  begin
     stream:=TFileStream.Create(Self.GetClientSavePath(self.AppName,self.ReportName),fmOpenRead or fmShareDenyWrite);
     Result:=stream;
  end
  else
  begin
     stream:=TStringStream.Create(UTF8Encode(Self.DesignText));
     Result:=stream;
  end;
end;

function TReportDataLoader.GetResponseText: string;
begin
  Result:=self.ReponseText;
end;

procedure TReportDataLoader.Load;
begin
  try
    HttpClient:=THttpCli.Create(nil);
    HttpClient.OnRequestDone:=self.OnRequestDone;
    HttpClient.URL:=Self.URL;
    HttpClient.SendStream:=Self.HttpSendStream;
    HttpClient.SendStream.Seek(0,soBeginning);
    HttpClient.RcvdStream:=Self.HttpReceStream;
    HttpClient.Post
  except
//    on e:Exception do
//    begin
//      ShowMessage(e.message);
//    end;
  end;
end;

procedure TReportDataLoader.OnRequestDone(Sender: TObject; RqType: THttpRequest; ErrCode: Word);
var
  reg:TPerlRegEx;
  list:TStrings;
begin
  try
  if ErrCode <> 0 then
  begin
     ShowMessage('连接报表服务器异常，请检查网络或重试...');
  end;
  if HttpClient.StatusCode <> 200 then
  begin
    ShowMessage('服务器异常，请联系管理员...');
  end;
  HttpClient.RcvdStream.Seek(0,0);
  SetLength(ReponseText,HttpClient.RcvdStream.size);
  HttpClient.RcvdStream.Read(ReponseText[1], Length(ReponseText));

  //处理数据
  if self.OnlyData then
  begin
    Self.DesignText:='';
    self.DataText:=TSysCommon.DecodeZipBase64(Self.ReponseText);
  end
  else
  begin
    list:=TStringList.Create;
    reg:=TPerlRegEx.Create;
    reg.Subject:=Self.ReponseText;
    reg.RegEx:='\{1234567890\}';
    reg.Split(list,2);
    Self.DesignText:=TSysCommon.DecodeZipBase64(list.Strings[0]);
    Self.DataText:=TSysCommon.DecodeZipBase64(list.Strings[1]);
    FreeAndNil(reg);
    list.Free;
  end;
  except
//    on e:Exception do
//    begin
//      ShowMessage(e.message);
//    end;
  end;
end;
class function TReportDataLoader.GetClientSaveRootDirectory: string;
begin
  Result:=GetEnvironmentVariable('USERPROFILE')+'\WebPrinter';
end;
class function TReportDataLoader.GetClientSaveDirectory(AappName: WideString): string;
var
  d:WideString;
begin
  d:=GetEnvironmentVariable('USERPROFILE')+'\WebPrinter\'+AappName;
  Result:=d;
end;

class function TReportDataLoader.GetClientSavePath(AappName,AreportName: WideString): string;
var
  p:WideString;
begin
  p:=GetEnvironmentVariable('USERPROFILE')+'\WebPrinter\'+AappName+'\'+AreportName+'.fr3';
  Result:=p;
end;
destructor TReportDataLoader.destroy;
begin
  Self.HttpClient.SendStream.Free;
  self.HttpClient.RcvdStream.Free;
  Self.HttpClient.Free;


//  Self.HttpSendStream.Free;
//  Self.HttpReceStream.Free;
//  Self.HttpClient.Free;
//
//    HttpClient.OnRequestDone := nil;
//  HttpClient.SendStream:=nil;
//  HttpClient.RcvdStream:=nil;
//  Self.HttpSendStream.Free;
//  Self.HttpReceStream.Free;
//  HttpClient.Free;

end;

function TReportDataLoader.EncodeParams(AParams: string): string;
var
  i:Integer;
  tmp:string;
  tmp1,tmp2:TStringList;
  reg1,reg2:TPerlRegEx;
begin
  tmp:='';
  tmp1:=TStringList.Create;
  tmp2:=TStringList.Create;
  reg1:=TPerlRegEx.Create;
  reg2:=TPerlRegEx.Create;
  reg1.Subject:=AParams;
  reg1.RegEx:='&';
  reg2.RegEx:='=';
  reg1.Split(tmp1,MaxInt);

  for  i:=0 to tmp1.Count-1 do
  begin
    reg2.Subject:=tmp1.Strings[i];
    reg2.Split(tmp2,2);
    if tmp2.Count=2 then
    tmp:=tmp+UrlEncode(UTF8Encode(tmp2.Strings[0]))+'='+UrlEncode(UTF8Encode(tmp2.Strings[1]))+'&';
    tmp2.Clear;
  end;
  FreeAndNil(tmp1);
  FreeAndNil(tmp2);
  FreeAndNil(reg1);
  FreeAndNil(reg2);
  Result:=tmp;
end;

function TReportDataLoader.UrlEncodeParams(AUrl: string): string;
var
  ipos:Integer;
  tmp:string;
  url,params:WideString;
begin
  url:=WideString(AUrl);
  ipos:=Pos('?',url);
  if ipos <> 0 then
  begin
    params:=RightStr(url,Length(url)-ipos);
    tmp:=LeftStr(url,ipos) + Self.EncodeParams(params);
    tmp:=LeftStr(tmp,Length(tmp)-1)
  end
  else
  begin
    tmp:=url;
  end;
  Result:=tmp;
end;

end.
