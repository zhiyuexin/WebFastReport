object WebFastReportFrm: TWebFastReportFrm
  Left = 246
  Top = 182
  Width = 591
  Height = 250
  Caption = 'WebFastReportFrm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 32
    Width = 112
    Height = 13
    Caption = 'FastReport'#27169#29256#25991#20214#65306
  end
  object Label2: TLabel
    Left = 40
    Top = 80
    Width = 112
    Height = 13
    Caption = 'FastReport'#25968#25454#25991#20214#65306
  end
  object Edit1: TEdit
    Left = 152
    Top = 24
    Width = 369
    Height = 21
    TabOrder = 0
    Text = 'D:\DpWorkSpace\WebFastReport\FastReport\test.fr3'
    OnDblClick = Edit1DblClick
  end
  object Edit2: TEdit
    Left = 152
    Top = 72
    Width = 369
    Height = 21
    TabOrder = 1
    Text = 'D:\DpWorkSpace\WebFastReport\FastReport\data.xml'
    OnDblClick = Edit2DblClick
  end
  object Button1: TButton
    Left = 176
    Top = 128
    Width = 75
    Height = 25
    Caption = #35774#35745
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 272
    Top = 128
    Width = 75
    Height = 25
    Caption = #39044#35272
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button4: TButton
    Left = 360
    Top = 128
    Width = 75
    Height = 25
    Caption = #25171#21360
    TabOrder = 4
    OnClick = Button4Click
  end
  object OpenDialog1: TOpenDialog
    Left = 384
    Top = 160
  end
  object OpenDialog2: TOpenDialog
    Left = 424
    Top = 160
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    Restrictions = []
    RTLLanguage = False
    Left = 488
    Top = 160
  end
  object frxReport1: TfrxReport
    Version = '4.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #39044#35774
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41084.930970312500000000
    ReportOptions.LastChange = 41084.930970312500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 456
    Top = 160
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object frxXMLExport1: TfrxXMLExport
    UseFileCache = True
    ShowProgress = True
    Background = True
    Creator = 'FastReport'
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 312
    Top = 168
  end
end
